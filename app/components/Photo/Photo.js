import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import photoWrapper from './photoWrapper';

function Photo({data}) {
    return (
        <photoWrapper id={data.id}>
            <Link
                className="singleImg"
                to={`photo?albumId=${data.albumId}&thumbnailUrl=${data.thumbnailUrl}&url=${data.url}&title=${data.title}&id=${data.id}`}
            >
                <img src={data.thumbnailUrl}/>
            </Link>
        </photoWrapper>
    );
}

Photo.propTypes = {
    data: PropTypes.object,
};

export default Photo;
