import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';
import AlbumCardWrapper from './AlbumCardWrapper';

function AlbumCard({id, title}) {
    return (
        <AlbumCardWrapper>
            <div className="album-number">
                <span> {id} </span>
            </div>
            <Link
                className="album-title"
                to={`photos?albumId=${id}`}
            >
                { title }
            </Link>
        </AlbumCardWrapper>
    );
}

AlbumCard.propTypes = {
    title: PropTypes.string,
    id: PropTypes.number
};

export default AlbumCard;

