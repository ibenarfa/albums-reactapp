import styled from 'styled-components';

const AlbumCardWrapper = styled.div`
    background: radial-gradient(ellipse at center, #c0c0c0 0%, #000000 76%, #000000 100%);
    display: inline-block;
    height: 300px;
    width: 300px;
    border-radius: 100%;
    margin:15px;

    .album-number{
        position: relative;
        text-align: center;
        top: 86px;
        font-size: 30px;
        color: black;
    }
    .album-number span{
        background: rgba(255, 255, 255, 0.66);
        width: 50px;
        height: 50px;
        display: block;
        margin: auto;
        line-height: 1.7;
        border-radius: 50px;
    }

    .album-title{
        padding:5px;
        text-decoration: none;
        position: relative;
        top: 110px;
        text-align: center;
        display: block;
        font-size: 20px;
        text-transform: capitalize;
        font-family: cursive;
        background: rgba(255, 255, 255, 0.66);
        color: #130707;
    }
`;

export default AlbumCardWrapper;
