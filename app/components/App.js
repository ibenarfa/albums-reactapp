import React from 'react';
import Routes from '../routes';
import HeaderContainer from '../containers/HeaderContainer';

const App = () =>
    <div>
        <HeaderContainer />
        { Routes }
    </div>;

export default App;
