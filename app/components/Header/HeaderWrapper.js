import styled from 'styled-components';

const HeaderWrapper = styled.header`
    position: fixed; 
    top: 0; 
    width: 100%; 
    background-color: #151212;
    box-shadow: 5px 2px 8px 0px rgba(171,169,171,0.72);
    z-index: 10;
    .menu {
        display: flex;
        justify-content: space-around;
    }
    li{
        list-style: none;
    }
    a{
        text-decoration: none;
        font-size: 25px;
        color: #626262;
    }
    .openPage{
        border-bottom: 4px solid #4CAF50;
        color: white;
    }
    .closePage{
        border-bottom: 4px solid rgba(226, 46, 46, 0.6);
    }
`;

export default HeaderWrapper;
