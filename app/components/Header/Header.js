import PropTypes from 'prop-types';
import React from 'react';
import HeaderWrapper from './HeaderWrapper';
import { Link } from 'react-router-dom';

const links = [
    {id: 1, to: '/', text: 'Albums'},
    {id: 2, to: '/wish-list', text: 'Wishlist'},
];

function Header({ changePage, pageId }) {
    return (
        <HeaderWrapper>
            <nav>
                <ul className="menu">
                    {links.map(link => (
                        <li key={link.id} onClick={() => changePage(link.id)}>
                            <Link
                                to={link.to}
                                title={link.text}
                                className={(pageId === link.id) ? 'openPage' : 'closePage'}
                            >
                                {link.text}
                            </Link>
                        </li>
                    ))}
                </ul>
            </nav>
        </HeaderWrapper>
    );
}

Header.propTypes = {
    pageId: PropTypes.number,
    changePage: PropTypes.func,
};

export default Header;

