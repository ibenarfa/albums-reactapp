import PropTypes from 'prop-types';
import React from 'react';
import AlbumContainer from '../../containers/AlbumsContainer';

const PhotoList = ({ location }) => {
    let path = `${location.pathname}`;
    if(location.pathname !== '') path += `${location.search}`;

    return (
        <AlbumContainer path = {path} />
    );
};

PhotoList.propTypes = {
    location: PropTypes.object,
};

export default PhotoList;

