import React from 'react';
import WishListContainer from '../../containers/WishListContainer/WishListContainer';

const WishList = () => {
    return (
        <WishListContainer />
    );
};

export default WishList;
