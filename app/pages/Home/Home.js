import React from 'react';
import AlbumContainer from '../../containers/AlbumsContainer';

const Home = () => {
    return (
        <div>
          <AlbumContainer path = {'/albums'} />
        </div>
    );
};

export default Home;

