import React from 'react';
import PropTypes from 'prop-types';
import PhotoContainer from '../../containers/PhotoContainer/PhotoContainer';

const Photo = ({location}) => {
    return (
        <PhotoContainer str = {location.search} />
    );
};

Photo.propTypes = {
    location: PropTypes.object,
};

export default Photo;
