import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home/Home';
import PhotoList from './pages/PhotoList/PhotoList';
import WishList from './pages/WishList/WishList';
import Photo from './pages/Photo/Photo';

export default (
    <div>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/photos" component={PhotoList}/>
            <Route path="/wish-list" component={WishList}/>
            <Route path="/photo" component={Photo}/>
        </Switch>
    </div>
);
