import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as types from '../actions/types';

const page = (state = 1, action) => {
    switch (action.type) {
        case types.OPEN_PAGE:
            return action.id;
        default:
            return state;
    }
};

function singlePhotoDetails(state = {}, action) {
    switch (action.type) {
        case types.PHOTO_DETAILS:
            return action.payload;
        default:
            return state;
    }
}

function wishListPhotos(state = [], action) {
    switch (action.type) {
        case types.WISH_LIST:
            return [
                ...state, action.data
            ];
        default:
            return state;
    }
}

function posts(
    state = {
        isFetching: false,
        items: []
    },
    action
) {
    switch (action.type) {
        case types.REQUEST_ALBUMS_OR_PHOTOS:
            return Object.assign({}, state, {
                isFetching: true,
            });
        case types.RECEIVE_ALBUMS_OR_PHOTOS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.receivedData,
            });
        default:
            return state;
    }
}

function getAlbumsOrPhtosByPath(state = {}, action) {
    switch (action.type) {
        case types.REQUEST_ALBUMS_OR_PHOTOS:
        case types.RECEIVE_ALBUMS_OR_PHOTOS:
            return Object.assign({}, state, {
                result: posts(state[action.path], action)
            });
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    singlePhotoDetails,
    getAlbumsOrPhtosByPath,
    page,
    wishListPhotos,
    routing
});

export default rootReducer;
