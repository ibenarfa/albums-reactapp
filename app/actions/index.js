import * as types from './types';
import fetch from 'isomorphic-fetch';

export function OpenPage(id) {
    return {
        type: types.OPEN_PAGE,
        id,
    };
}

export function photoDetails(payload) {
    return {
        type: types.PHOTO_DETAILS,
        payload
    };
}

export function addToWhishList(data) {
    return {
        type: types.WISH_LIST,
        data
    };
}

function requestPosts(path) {
    return {
        type: types.REQUEST_ALBUMS_OR_PHOTOS,
        path
    };
}

function receivePosts(path, data) {
    return {
        type: types.RECEIVE_ALBUMS_OR_PHOTOS,
        path,
        receivedData: data,
    };
}

function fetchQuery(path) {
    return (dispatch) => {
        dispatch(requestPosts(path));
        return fetch(`https://jsonplaceholder.typicode.com${path}`)
        .then(
            response => response.json(),
            error => console.log('An error occured.', error)
        )
        .then(json =>
            dispatch(receivePosts(path, json))
        );
    };
}

function shouldFetch(state, path) {
    const posts = state.getAlbumsOrPhtosByPath[path];
    if (!posts) {
        return true;
    } else if (posts.isFetching) {
        return false;
    }
    return null;
}

export function fetshPhotosOrAlbums(path) {
    return (dispatch, getState) => {
        if (shouldFetch(getState(), path)) {
            return dispatch(fetchQuery(path));
        }
        return null;
    };
}
