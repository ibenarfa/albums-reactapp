import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { fetshPhotosOrAlbums } from '../actions';
import AlbumCard from '../components/AlbumCard/AlbumCard';
import Photo from '../components/Photo/Photo';

const styledDiv = {
    marginTop: '100px',
};

const styledTitle = {
    fontSize: '30px',
    margin: '0 50px',
    fontWeight: 'bold',
    borderBottom: '2px solid gray',
    paddingLeft: '50px',
};

const albumsContainer = {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    marginTop: '10px',
};

class AlbumContainer extends React.Component {
    static propTypes = {
        data: PropTypes.object,
        getPosts: PropTypes.func,
        path: PropTypes.string,
    };
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const { getPosts, path } = this.props;
        getPosts(path);
    }
    render() {
        const { data, path } = this.props;
        return(
            <div style={styledDiv}>
                <div style={styledTitle}>
                    {
                        (path === '/albums') ? 'Albums' : 'Photos'
                    }
                </div>
                <div style={albumsContainer}>
                    { path === '/albums' &&
                        (data.result && data.result.isFetching === false) ?
                        data.result.items.map(item => <AlbumCard key={item.id} id={item.id} title={item.title}/>)
                        : ''
                    }
                    {
                        (data.result && data.result.isFetching === false) ?
                        data.result.items.map(item => <Photo key={item.id} data={item} />)
                        : ''
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.getAlbumsOrPhtosByPath
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getPosts: path => dispatch(fetshPhotosOrAlbums(path))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AlbumContainer);
