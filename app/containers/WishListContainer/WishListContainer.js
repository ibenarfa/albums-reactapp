import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import WishListContainerWrapper from './WishListContainerWrapper';

const WishListContainer = ({data}) => {
    return (
        <WishListContainerWrapper>
            {
                (data.length > 0) ?
                    data.map(item => <div key={item.id}>
                        <img alt="" src={item.url} />
                    </div>)
                : <div className="empty" > You Don't Have Any Photo in Your WishList </div>
            }
        </WishListContainerWrapper>
    );
};


WishListContainer.propTypes = {
    data: PropTypes.array,
};

const mapStateToProps = (state) => {
    return {
        data: state.wishListPhotos
    };
};

export default connect(
    mapStateToProps
)(WishListContainer);
