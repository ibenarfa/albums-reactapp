import styled from 'styled-components';

const WishListContainerWrapper = styled.div`
    display: flex;
    justify-content: center;
    margin: 20px;
    margin-top: 100px;
    flex-wrap: wrap;

    .empty{
        background: rgb(0, 0, 0);
        width: 500px;
        height: 200px;
        color: white;
        font-size: 30px;
        text-align: center;
        line-height: 2;
        padding-top: 77px;
        border-radius: 20px;
    }
`;

export default WishListContainerWrapper;
