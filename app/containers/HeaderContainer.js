import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { OpenPage } from '../actions';
import Header from '../components/Header/Header';

const HeaderContainer = ({ changePage, pageId }) => {
    return (
        <div>
            <Header changePage={changePage} pageId={pageId} />
        </div>
    );
};

HeaderContainer.propTypes = {
    pageId: PropTypes.number,
    changePage: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        pageId: state.page
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        changePage: id => dispatch(OpenPage(id))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HeaderContainer);
