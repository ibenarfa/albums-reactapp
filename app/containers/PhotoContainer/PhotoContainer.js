import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import { addToWhishList } from '../../actions';
import PhotoContainerWrapper from './PhotoContainerWrapper';

const getParams = (str) => {
    const ObjParams = {};
    const arrayParam = str.split('?').pop().split('&');
    arrayParam.map(item => {
        const ObjVal = item.split('=').pop();
        ObjParams[item.split('=')[0]] = decodeURIComponent(ObjVal);
    });
    return ObjParams;
};

const  checkArray = ( obj, arr ) => {
    let flag = true;
    for ( let i = 0; i < arr.length; i++ ) {
        if ( obj.id === arr[i].id) {
            flag = false;
            break;
        }
    }
    return flag;
};

const PhotoContainer = ({ str, whishList, wishListPhotos }) => {
    const data = getParams(str);
    return (
        <PhotoContainerWrapper>
            <img src= {data.url} />
            <div className="imgDetails">
                <div>
                    <p>
                        <span>Title: </span>
                        {
                            data.title
                        }
                    </p>
                    <p>
                        <span>Album: </span>
                       {
                            data.albumId
                        }
                    </p>
                </div>
                <button onClick={() => {
                    checkArray(data, wishListPhotos) ? whishList(data) : alert('This photo already exist');
                }}>
                    Add to Wish list ({wishListPhotos.length})
                </button>
            </div>
        </PhotoContainerWrapper>
    );
};

PhotoContainer.propTypes = {
    whishList: PropTypes.func,
    wishListPhotos: PropTypes.array,
    str: PropTypes.string,
};

const mapStateToProps = (state) => {
    return {
        wishListPhotos: state.wishListPhotos
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        whishList: data => dispatch(addToWhishList(data))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PhotoContainer);

