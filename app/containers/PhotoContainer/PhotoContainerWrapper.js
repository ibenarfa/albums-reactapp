import styled from 'styled-components';

const PhotoContainerWrapper = styled.div`
    display: flex;
    margin:20px;
    margin-top: 100px;
    justify-content: space-around;
    font-size: 25px;
    font-family: cursive;
    
    .imgDetails{
        margin-left:20px;
    }
    p {
        color: gray;
    }
    span {
        color: black;
    }
    button{
        background: #4CAF50;
        border: none;
        width: 200px;
        height: 50px;
        border-radius: 20px;
        color: white;
        font-size: 20px;
        font-weight: 600;
        cursor: pointer;
    }
    button:hover{
        background:#009688;
    }
`;

export default PhotoContainerWrapper;
